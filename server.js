const express = require('express'),
    sassMiddleware = require('node-sass-middleware');

const app = express();

app.use(sassMiddleware({
    /* Options */
    src: 'scss',
    dest: 'public',
    debug: true,
    outputStyle: 'compressed',
    force: true
}));

app.use(express.static('public'));

app.listen(3000);