# tthf-simple-dark

A simple dark theme for https://tthfanfic.org.
Aims to also fix some small issues on small devices.

## Usage

Go to https://www.tthfanfic.org/customize.php

In the form at the end enter the following:

Custom Stylesheet URL: https://khartir.gitlab.io/tthf-simple-dark/simple-dark.css

Custom Icons Folder URL: /images/skins/fire

Leave Custom Banner Image URL emtpy


